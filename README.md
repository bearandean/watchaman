# Watchaman
An Awesome security system using only your old laptop or desktop and a web cam

...but really: 
An excercise in simple object detection/segmentation using neural networks.
---

## Table of contents
* [General info](#general-info)
* [Technologies](#technologies)
* [Getting started](#getting-started)
* [Setup](#setup)
* [Installing](#installing)
* [Features](#features)
* [Status](#status)
* [Versioning](#versioning)
* [Authors](#authors)
* [Aknowledgements](#acknowledgements)

---

## General info

A Python based module that uses different Machine Learning and Computer Vision modules to segment animals, a set of objects or people in camera view. 

## Technologies
 Minimum requirements includes Python 3+ and the modules:

```
matplotlib==3.3.2
numpy==1.19.2
opencv-python==3.4.8.29
Pillow==7.2.0
pkg-resources==0.0.0
pyparsing==2.4.7
python-dateutil==2.8.1
scipy==1.5.2
six==1.15.0
torch==1.6.0+cpu
torchvision==0.7.0+cpu
```

## Getting Started

Using a web cam and a Linux based sytem (up to now), plug the camera or use the laptop cam, and assuming the package manager *pip* is installed, in a terminal excute the command:

```
pip install -r /path/to/requirements.txt
```

This will install the required modules. 
I suggest doing this in an evironment created with modules sucha as *venv*

### Prerequisites

A python 3.+ installation and surely a linux system as I haven't tested in Windows

```
e.g. LinuxMint 16.+, Ubuntu 18.+
```

### Setup
setup your local environement using a virtual environment *eg. venv*

#### Installing

Open a terminal and after cloning this repo using the approrpiate git command, type or copy paste:

```
pip install -r /path/to/requirements.txt
```

After that, just type

```
python watchaman.py
```
Now check out how it catches all those pestering intruders in the back lane... get jobs hippies!

It'll show feed from your first available web camera in the system and the segmentations of the identified objects.

### Running the tests

In progress

#### Break down into end to end tests

Explain what these tests test and why

```
Give an example
```

## Features
List of features ready and TODOs for future development
* Awesome feature 1
* Awesome feature 2
* Awesome feature 3

To-do list:
* Wow improvement to be done 1
* Wow improvement to be done 2

## Status
Project is: _in progress_, _finished_, _no longer continue_ and why?


### Versioning


## Authors

* **Andres Medina** - *Initial work*

See also the list of [contributors] who participated in this project.

## License


## Acknowledgments

* Hat tip to anyone whose code was used
* Inspiration
* etc
