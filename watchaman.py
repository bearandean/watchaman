#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Sep 18 11:25:29 2019

@author: andres
"""

import cv2
import os
import numpy as np
import torch
from torchvision import models
import torchvision.transforms as T
from PIL import Image
import matplotlib.pyplot as plt

def label_str(l):
    labels = {0 : "background",
              1 : "aeroplane", 2 : "bicycle", 3 : "bird", 4 : "boat", 5 : "bottle",
              6 : "bus", 7 : "car", 8 : "cat", 9 : "chair", 10 : "cow",
              11: "dining table", 12 : "dog", 13 : "horse", 14 : "motorbike", 15 : "person",
              16: "potted plant", 17 : "sheep", 18 : "sofa", 19 : "train", 20:"tv/monitor"}
    return labels[l]

def decode_segmap(image, nc=21):
    label_colors = np.array([(0, 0, 0), # 0 = background
             # 1=aeroplane, 2=bicycle, 3=bird, 4=boat, 5=bottle
             (128, 0, 0), (0, 128, 0), (128, 128, 0), (0, 0, 128), (128, 0, 128),
             # 6=bus, 7=car, 8=cat, 9=chair, 10=cow
             (0, 128, 128), (128, 128, 128), (64, 0, 0), (192, 0, 0), (64, 128, 0),
             # 11=dining table, 12=dog, 13=horse, 14=motorbike, 15=person
             (192, 128, 0), (64, 0 ,128), (192, 0, 128), (64, 128, 128), (192, 128, 128),
             # 16=potted plant, 17=sheep, 18=sofa, 19=train, 20=tv/monitor
             (0, 64, 0), (128, 64, 0), (0, 192, 0), (128, 192, 0), (0, 64, 128)])
    r = np.zeros_like(image).astype(np.uint8)
    g = np.zeros_like(image).astype(np.uint8)
    b = np.zeros_like(image).astype(np.uint8)

    for l in range(0, nc):
        idx = image == l
        r[idx] = label_colors[l, 0]
        g[idx] = label_colors[l, 1]
        b[idx] = label_colors[l, 2]
    rgb = np.stack([r, g, b], axis=2)
    return rgb

def segment(net, img, show_orig=True):
    img = Image.fromarray(img)
    #img = img.convert("RGB")
    if show_orig: plt.imshow(img); plt.axis('off')
    # Comment the Resize and CenterCrop for better inference results
    trf = T.Compose([T.Resize(64), 
                   #T.CenterCrop(224), 
                   T.ToTensor(), 
                   T.Normalize(mean = [0.485, 0.456, 0.406], 
                               std = [0.229, 0.224, 0.225])])
    inp = trf(img).unsqueeze(0)
    out = net(inp)['out']
    om = torch.argmax(out.squeeze(), dim=0).detach().cpu().numpy()
    labels_found = np.unique(om)
    found = []
    for l in labels_found:
        if l:
            print("    {:}".format(label_str(l)))
        found.append(label_str(l))
    plt.show()
    rgb = decode_segmap(om)
    return rgb, found

def list_ports():
    """
    Test the ports and returns a tuple with the available ports and the ones that are working.
    """
    is_working = True
    dev_port = 0
    working_ports = []
    available_ports = []
    while is_working:
        camera = cv2.VideoCapture(dev_port)
        if not camera.isOpened():
            is_working = False
            print("Port %s is not working." %dev_port)
        else:
            is_reading, img = camera.read()
            w = camera.get(3)
            h = camera.get(4)
            if is_reading:
                print("Port %s is working and reads images (%s x %s)" %(dev_port,h,w))
                working_ports.append(dev_port)
            else:
                print("Port %s for camera ( %s x %s) is present but does not reads." %(dev_port,h,w))
                available_ports.append(dev_port)
        dev_port +=1
    return available_ports,working_ports
def main():
    fcn = models.segmentation.fcn_resnet101(pretrained=True).eval()
    print("pretrained model loaded")
    # list and use port for frames capture
    _, wport = list_ports()
    
    if len(wport):
        camera = cv2.VideoCapture(wport[0])
        alpha = 0.4
        while True:
            ret, img0 = camera.read()
            img_s = [int(0.5*i)for i in img0.shape]
            img = np.zeros((img_s[0],img_s[1],3), dtype = np.uint8)
            for i in range(3):
                img[:,:,i] = cv2.resize(img0[:,:,i], ((img_s[1],img_s[0]))).astype(np.uint8)
    
            # proces img here
            rgb, found_labels = segment(fcn, img, False)
            output = rgb
            thumbo = np.zeros(rgb.shape)
            for i in range(3):
                thumbo[:,:,i] = cv2.resize(img[:,:,i], (rgb.shape[1], rgb.shape[0]))
            output = cv2.addWeighted(rgb.astype(np.uint8), alpha, thumbo.astype(np.uint8), 1 - alpha, 0)
            # display on screen
            cv2.imshow("image", output)
            # manage window closing
            key = cv2.waitKey(1) & 0xFF
            if key == ord("q") or key == 27: # press 'esc' or 'q' on keyboard
                break
        # close window when done
        cv2.destroyAllWindows()
        
if __name__ == "__main__":
    main()

